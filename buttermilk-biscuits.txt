! Buttermilk Biscuits

#flour
Mix in a food processor:
  2   | cups | all-purpose flour
  1   | tbsp | baking powder
  1/4 | tsp  | baking soda
  2   | tsp  | sugar
  1   | tsp  | salt
  6   | tbsp | cold butter [cubed]

#dough
Slowly pour and mix into #flour:
  3/4 | cup | buttermilk [cold]

Flatten #dough on lightly floured surface

Fold #dough into 3rds 3 times and flatten to 1/2 inch

#biscuits
Cut #dough into to desired size

Brush onto #biscuits:
  1/4 | cup | buttermilk

Bake #biscuits @ 450 for 00:15:00 - 00:17:00

https://www.livewellbakeoften.com/wprm_print/16759
